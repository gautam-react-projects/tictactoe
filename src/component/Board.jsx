"use strict";
import React, { useState } from "react";
import Box from "./Box";

const initialState = Array(9).fill(null);
export default function Board() {
  let offline = false;
  const [move, setmove] = useState(Array(9).fill(null));

  const [Turn, setTurn] = useState(true);

  function isDraw() {
    if (!move.includes(null)) {
      console.log("Match Drawed");
      return true;
    }
  }

  function WinnerCheck() {
    const WinSenarios = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let senario of WinSenarios) {
      let [a, b, c] = senario;

      if (!offline) {
        if (move[a] != null && move[a] === move[b] && move[b] === move[c]) {
          offline = true;
          return move[a];
        }
      }
    }
    return false;
  }
  const winner = WinnerCheck();
  function HandleClick(index) {
    if (move[index] != null) {
      return;
    }
    // if(WinnerCheck()){
    //   return;
    // }
    const nextMove = [...move];

    nextMove[index] = Turn ? "X" : "O";

    setmove(nextMove);
    setTurn(!Turn);
  }
  function HandleReset() {
    setmove(initialState);
  }

  return (
    <div className="board">
      <div className="Title">
        <h1>TIC TAC TOE</h1>
      </div>
      {winner ? (
        <div className="winMessage">
          <div className="GameScreen">
            <h2> Congratulations!! </h2>
            <h2>{winner} has won the game</h2>
          </div>
          <button className="restartButton" onClick={HandleReset}>
            Restart Game
          </button>
        </div>
      ) : (
        <>
          {isDraw() && !WinnerCheck() ? (
            <div className="GameScreen">
              <h2>Match Draw</h2>
              <button className="restartButton" onClick={HandleReset}>
                Restart Game
              </button>
            </div>
          ) : (
            <></>
          )}

          <div className="boardrow">
            <Box onClick={() => HandleClick(0)} value={move[0]} />
            <Box onClick={() => HandleClick(1)} value={move[1]} />
            <Box onClick={() => HandleClick(2)} value={move[2]} />
          </div>
          <div className="boardrow">
            <Box onClick={() => HandleClick(3)} value={move[3]} />
            <Box onClick={() => HandleClick(4)} value={move[4]} />
            <Box onClick={() => HandleClick(5)} value={move[5]} />
          </div>
          <div className="boardrow">
            <Box onClick={() => HandleClick(6)} value={move[6]} />
            <Box onClick={() => HandleClick(7)} value={move[7]} />
            <Box onClick={() => HandleClick(8)} value={move[8]} />
          </div>
        </>
      )}
    </div>
  );
}
