import React from "react";

export default function Box(props) {
  return (
    <button className="box" onClick={props.onClick}>
      <h2>{props.value}</h2>
    </button>
  );
}
